locals {
  tags = {
    Environment = var.cluster_name
    Terraform   = true
  }
}

data "aws_availability_zones" "available" {
  state = "available"
}

resource "random_shuffle" "az" {
  input        = data.aws_availability_zones.available.names
  result_count = 3
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.33"

  name = "eks-${var.cluster_name}"
  cidr = "192.168.0.0/16"

  azs = [
    random_shuffle.az.result[0],
    random_shuffle.az.result[1],
  random_shuffle.az.result[2]]
  private_subnets = [
    "192.168.1.0/24",
    "192.168.2.0/24",
    "192.168.3.0/24"
  ]
  public_subnets = [
    "192.168.101.0/24",
    "192.168.102.0/24",
    "192.168.103.0/24"
  ]

  enable_nat_gateway = true

  tags = local.tags
}

resource "aws_security_group" "cluster_security_group" {
  name        = "eks-${var.cluster_name}"
  description = "Security groups for Kubernetes control plane"
  vpc_id      = module.vpc.vpc_id

  ingress {
    description = "connections to Kubernetes API"
    from_port   = 6443
    to_port     = 6443
    protocol    = "tcp"
    cidr_blocks = var.cluster_whitelisted_cids
  }

  tags = local.tags
}


module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "11.1.0"

  cluster_name    = var.cluster_name
  cluster_version = "1.15"

  vpc_id = module.vpc.vpc_id
  subnets = [
    module.vpc.private_subnets[0],
    module.vpc.private_subnets[1],
    module.vpc.private_subnets[2]
  ]

  cluster_security_group_id = aws_security_group.cluster_security_group.id

  map_roles = [for m in var.cluster_admin_role_arns : {
    rolearn  = m.role_arn
    username = m.role_arn
    groups   = ["system:masters"]
  }]
  map_users = [for m in var.cluster_admin_user_arns : {
    userarn  = m.user_arn
    username = m.user_arn
    groups   = ["system:masters"]
  }]

  worker_groups = [
    {
      instance_type = var.workers_instance_type
      asg_max_size  = var.workers_asg_max
    }
  ]

  tags = local.tags
}

resource "local_file" "kubeconfig" {
  content  = module.eks.kubeconfig
  filename = var.kubeconfig_path
}

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.cluster.token

  # avoid loading a config file, or else it might load one accidentally referenced in $KUBECONFIG
  # and change the wrong cluster!
  load_config_file = false
}
