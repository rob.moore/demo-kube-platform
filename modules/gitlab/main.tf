locals {
  tags = {
    Environment = var.runner_name
    Terraform   = true
  }
}

data "aws_availability_zones" "available" {
  state = "available"
}

data "aws_region" "current" {}

resource "random_shuffle" "az" {
  input        = data.aws_availability_zones.available.names
  result_count = 1
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.33"

  name = "vpc-${var.runner_name}"
  cidr = "192.168.0.0/16"

  enable_nat_gateway      = true
  azs                     = [random_shuffle.az.result[0]]
  public_subnets          = ["192.168.100.0/24"]
  private_subnets         = ["192.168.101.0/24"]
  enable_s3_endpoint      = false
  map_public_ip_on_launch = true

  tags = local.tags
}

module "runner" {
  source = "git::https://github.com/npalm/terraform-aws-gitlab-runner.git?ref=4.15.0"

  aws_region  = data.aws_region.current.name
  aws_zone    = random_shuffle.az.result[0]
  environment = var.runner_name

  cloudwatch_logging_retention_in_days = 1
  enable_forced_updates                = true
  instance_type                        = var.instance_type
  ssh_key_pair                         = var.ssh_keypair

  runners_use_private_address = false
  enable_eip                  = false

  vpc_id                   = module.vpc.vpc_id
  subnet_ids_gitlab_runner = var.use_private_subnet ? module.vpc.private_subnets : module.vpc.public_subnets
  subnet_id_runners        = element(var.use_private_subnet ? module.vpc.private_subnets : module.vpc.public_subnets, 0)

  runners_executor   = "docker"
  runners_name       = var.runner_name
  runners_gitlab_url = var.gitlab_url

  gitlab_runner_registration_config = {
    registration_token = var.registration_token
    tag_list           = ""
    description        = var.runner_name
    locked_to_project  = true
    run_untagged       = true
    maximum_timeout    = 3600
  }

  tags = local.tags
}
