output "nat_ip" {
  value = format("%s/32", module.vpc.nat_public_ips[0])
}
