variable "runner_name" {
  description = "Name for this gitlab-runner used in labels"
  type        = string
}

variable "use_private_subnet" {
  description = "Whether to run the gitlab-runner in a private subnet"
  type        = bool
  default     = true
}

variable "ssh_keypair" {
  description = "Optional EC2 keypair to attach to runner (for debugging only)"
  type        = string
  default     = ""
}

variable "instance_type" {
  description = "EC2 instance type for runner"
  type        = string
  default     = "t3.nano"
}

variable "gitlab_url" {
  description = "URL of the gitlab instance to connect to."
  type        = string
  default     = "https://gitlab.com"
}

variable "registration_token" {
  description = "Token to authenticate this runner with a Gitlab project"
  type        = string
}
