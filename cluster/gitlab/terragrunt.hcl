terraform {
  source = "../../modules/gitlab"
}

locals {
  cluster_vars = read_terragrunt_config(find_in_parent_folders("variables.hcl"))
  cluster_name = local.cluster_vars.locals.cluster_name
}

include {
  path = find_in_parent_folders()
}

inputs = {
  runner_name = "${local.cluster_name}-gitlab-runner"
}
