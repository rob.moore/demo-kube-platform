# defines global variables necessary for each cluster under the "clusters" directory
locals {
  # general settings
  account_name = "myaccount"
  aws_region   = "us-east-2"
  cluster_name = "${local.account_name}-eks"

  # kubernetes settings
  kube_iam_trusted_roles     = []
  kube_iam_trusted_users     = ["arn:aws:iam::123456789012:user/myuser"] # replace this with your AWS IAM user
  kube_whitelist_ip_cidrs    = ["w.x.y.z/32"]                       # replace this with your IP address
  kube_workers_asg_max       = 1
  kube_workers_instance_type = "m5.large"
}
