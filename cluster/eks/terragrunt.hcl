terraform {
  source = "../../modules/eks"
}

locals {
  cluster_vars = read_terragrunt_config(find_in_parent_folders("variables.hcl"))

  cluster_admin_role_arns = [for r in local.cluster_vars.locals.kube_iam_trusted_roles : {
    role_arn = r
  }]
  cluster_admin_user_arns = [for u in local.cluster_vars.locals.kube_iam_trusted_users : {
    user_arn = u
  }]
  kubeconfig_path = "${get_parent_terragrunt_dir()}/artifacts/kubeconfig"
}

include {
  path = find_in_parent_folders()
}

dependencies {
  paths = [
    "../gitlab"]
}

dependency "gitlab" {
  config_path = "../gitlab"

  mock_outputs = {
    nat_ip = "1.2.3.4/32"
  }
}

inputs = {
  cluster_admin_user_arns  = local.cluster_admin_user_arns
  cluster_admin_role_arns  = local.cluster_admin_role_arns
  cluster_name             = local.cluster_vars.locals.cluster_name
  cluster_whitelisted_cids = concat(local.cluster_vars.locals.kube_whitelist_ip_cidrs, [
    dependency.gitlab.outputs.nat_ip
  ])
  kubeconfig_path          = local.kubeconfig_path
  workers_asg_max          = local.cluster_vars.locals.kube_workers_asg_max
  workers_instance_type    = local.cluster_vars.locals.kube_workers_instance_type
}
